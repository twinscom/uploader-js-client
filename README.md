# uploader-js-client

The reference implementation of a JavaScript client for uploader

[![build status](https://gitlab.com/twinscom/uploader-js-client/badges/master/build.svg)](https://gitlab.com/twinscom/uploader-js-client/builds)

## Requirements

- jQuery

## API

```javascript
var uploader = TWINSCOM.uploader.make({
    projectInstanceId: "project-instance-id",
    baseUri: "http://localhost/",
    apiKey: "api-key",
    acceptLanguage: "de" // optional
});

uploader.uploadFile({
    file: document.querySelector("#file-input").files[0],
    onSuccess: function (fileModel) {
        console.log(fileModel);
    },
    onError: function (message) {
        console.error(message);
    },
    onProgress: function (progress) {
        var percent = Math.round(100 / progress.total * progress.loaded);
        console.log(percent + "%");
    }
});

uploader.getFile({
    id: "file-id",
    onSuccess: function (fileModel) {
        console.log(fileModel);
    },
    onError: function (message) {
        console.error(message);
    }
});

uploader.getFiles({
    ids: [
        "file-id",
        "other-file-id"
    ],
    onSuccess: function (fileModels) {
        console.log(fileModels);
    },
    onError: function (message) {
        console.error(message);
    }
});

uploader.deleteFile({
    id: "file-id",
    onSuccess: function () {
        console.log("Deleted!");
    },
    onError: function (message) {
        console.error(message);
    }
});

```

## License

[MIT](LICENSE)
