/*global $ */
/*jslint browser, maxlen: 80 */
/*eslint camelcase: 0 */

/**
 * onSuccess callback that receives a file model.
 *
 * @callback onSuccessOne
 *
 * @param {Object} fileModel - File model.
 */

/**
 * onSuccess callback that receives an array of file models.
 *
 * @callback onSuccessMany
 *
 * @param {Object[]} fileModels - File models.
 */

/**
 * onSuccess callback that doesn't receive anything.
 *
 * @callback onSuccessEmpty
 */

/**
 * onError callback.
 *
 * @callback onError
 *
 * @param {string} message - Error message.
 */

/**
 * onProgress callback that receives ProgressEvent.
 *
 * @callback onProgress
 *
 * @param {Object} progressEvent - ProgressEvent.
 */

var TWINSCOM;

TWINSCOM = TWINSCOM || {};
TWINSCOM.uploader = {};

/**
 * Uploader constructor.
 *
 * @param {Object} options - Options.
 * @param {string} options.projectInstanceId - Project instance ID.
 * @param {string} options.baseUri - Uploader service's base URI.
 * @param {string} options.apiKey - Uploader service's API key.
 * @param {string=} options.acceptLanguage - The "Accept-Language" header.
 *
 * @returns {Object} - Uploader.
 */
TWINSCOM.uploader.make = function makeUploader(options) {
    "use strict";

    var apiVersion = "1.0.0";

    var projectInstanceId = options.projectInstanceId;
    var baseUri = options.baseUri;
    var apiKey = options.apiKey;
    var acceptLanguage = options.acceptLanguage;

    var defaultHeaders = {
        "Accept-Version": apiVersion
    };

    /**
     * Attaches callbacks to an AJAX request settings object if they exist.
     *
     * @param {Object} requestSettings - AJAX request settings.
     * @param {Object} callbacks - Callbacks.
     * @param {onSuccessOne=} callbacks.onSuccess
     * @param {onError=} callbacks.onError
     * @param {onProgress=} callbacks.onProgress
     *
     * @returns {Object} - AJAX request settings with callbacks.
     */
    function attachCallbacks(requestSettings, callbacks) {
        if (callbacks.onSuccess) {
            requestSettings.success = function callOnSuccess(result) {
                callbacks.onSuccess(result);
            };
        }
        if (callbacks.onError) {
            requestSettings.error = function callOnError(jqXhr, textStatus) {
                var message = (
                    jqXhr.responseJSON && jqXhr.responseJSON.message
                ) || textStatus;

                callbacks.onError(message);
            };
        }
        if (callbacks.onProgress) {
            requestSettings.xhr = function attachOnProgressCallback() {
                var xhr = new XMLHttpRequest();

                xhr.upload.addEventListener("progress", callbacks.onProgress);

                return xhr;
            };
        }

        return requestSettings;
    }

    /**
     * Builds a query string by combining default and passed parameters.
     *
     * @param {Object=} params - Query params.
     *
     * @returns {string} - The query string.
     */
    function buildQueryString(params) {
        var defaultParams = {
            project_instance_id: projectInstanceId,
            api_key: apiKey
        };

        if (params) {
            $.extend(defaultParams, params);
        }

        return $.param(defaultParams);
    }

    /**
     * Uploads a file.
     *
     * @param {Object} params - Params.
     * @param {File} params.file - File.
     * @param {onSuccessOne=} params.onSuccess
     * @param {onError=} params.onError
     */
    function uploadFile(params) {
        var file = params.file;
        var queryString = buildQueryString({
            name: file.name
        });
        var requestSettings = {
            method: "POST",
            url: baseUri + "files?" + queryString,
            headers: defaultHeaders,
            contentType: "application/octet-stream",
            data: file,
            processData: false
        };

        requestSettings = attachCallbacks(requestSettings, params);

        $.ajax(requestSettings);
    }

    /**
     * Gets a file model.
     *
     * @param {Object} params - Params.
     * @param {string} params.id - File ID.
     * @param {onSuccessOne=} params.onSuccess
     * @param {onError=} params.onError
     */
    function getFile(params) {
        var queryString = buildQueryString();
        var requestSettings = {
            method: "GET",
            url: baseUri + "files/" + params.id + "?" + queryString,
            headers: defaultHeaders
        };

        requestSettings = attachCallbacks(requestSettings, params);

        $.ajax(requestSettings);
    }

    /**
     * Gets a list of file models.
     *
     * @param {Object} params - Params.
     * @param {string} params.ids - File IDs.
     * @param {onSuccessMany=} params.onSuccess
     * @param {onError=} params.onError
     */
    function getFiles(params) {
        var queryString = buildQueryString({
            file_ids: params.ids.join()
        });
        var requestSettings = {
            method: "GET",
            url: baseUri + "files?" + queryString,
            headers: defaultHeaders
        };

        requestSettings = attachCallbacks(requestSettings, params);

        $.ajax(requestSettings);
    }

    /**
     * Deletes a file and its derivatives.
     *
     * @param {Object} params - Params.
     * @param {string} params.id - File ID.
     * @param {onSuccessEmpty=} params.onSuccess
     * @param {onError=} params.onError
     */
    function deleteFile(params) {
        var queryString = buildQueryString();
        var requestSettings = {
            method: "DELETE",
            url: baseUri + "files/" + params.id + "?" + queryString,
            headers: defaultHeaders
        };

        requestSettings = attachCallbacks(requestSettings, params);

        $.ajax(requestSettings);
    }

    if (acceptLanguage) {
        defaultHeaders["Accept-Language"] = acceptLanguage;
    }

    return Object.freeze({
        uploadFile: uploadFile,
        getFile: getFile,
        getFiles: getFiles,
        deleteFile: deleteFile
    });
};
